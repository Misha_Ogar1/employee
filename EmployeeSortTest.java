import java.util.ArrayList;
import java.util.Arrays;

public class EmployeeSortTest {
    public static void main(String[] args) {

        Employee[] staff = new Employee[3];

        staff[0] = new Employee("B", 11_000);
        staff[1] = new Employee("A", 10_000);
        staff[2] = new Employee("C", 12_000);

        Arrays.sort(staff);

        for (Employee e : staff) {
            System.out.println("Name: " + e.getName() + " " + "Salary: " + e.getSalary());
        }

    }

}
